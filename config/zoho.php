<?php
return [
    'token_url' => env('TOKEN_URL'),
    'client_id' => env('CLIENT_ID'),
    'client_secret' => env('CLIENT_SECRET'),
    'code' => env('CODE'),
    'grant_type_auth' => env('GRANT_TYPE_AUTH'),
    'grant_type_refresh' => env('GRANT_TYPE_REFRESH'),
    'access_token' => env('ACCESS_TOKEN'),
    'refresh_token' => env('REFRESH_TOKEN'),
    'invalid_token' => env('CODE_INVALID'),
];
