<?php

namespace App\Http\Controllers;

use App\Services\ZohoService;

class BaseController extends Controller
{
    public ZohoService $service;

    public function __construct(ZohoService $service)
    {
        $this->service = $service;
    }
}
