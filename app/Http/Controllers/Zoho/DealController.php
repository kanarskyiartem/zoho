<?php

namespace App\Http\Controllers\Zoho;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Http;

class DealController extends BaseController
{
    public function index(CaseController $caseController): void
    {
        $this->service->checkToken();
        $dealId = $this->store();
        $caseController->store($dealId);
    }

    public function store(): string
    {
        $params['data'] = array(
            array(
                'Deal_Name' => 'Zoho32312',
                'Stage' => 'qualification'
            )
        );

        $responseDeals = Http::withBody(json_encode($params), 'application/json')
            ->withHeaders(['Authorization' => 'Zoho-oauthtoken ' . config('zoho.access_token')])
            ->post('https://www.zohoapis.com/crm/v2/Deals')
            ->json();

        return $responseDeals['data'][0]['details']['id'];
    }
}
