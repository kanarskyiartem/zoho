<?php

namespace App\Http\Controllers\Zoho;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Http;

class CaseController extends BaseController
{
    public function store($dealId)
    {
        $params['data'] = array(
            array(
                'Case_Origin' => 'Email',
                'Status' => 'New',
                'Subject' => 'taskwithdeal',
                'Deal_Name' => array('id' => $dealId)
            )
        );

        Http::withBody(json_encode($params), 'application/json')
            ->withHeaders(['Authorization' => 'Zoho-oauthtoken ' . config('zoho.access_token')])
            ->post('https://www.zohoapis.com/crm/v2/Cases')
            ->json();
    }
}
