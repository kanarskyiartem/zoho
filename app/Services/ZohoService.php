<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class ZohoService
{
    public function refreshToken()
    {
        $paramsRefresh = array(
            'grant_type' => config('zoho.grant_type_refresh'),
            'client_id' => config('zoho.client_id'),
            'client_secret' => config('zoho.client_secret'),
            'refresh_token' => config('zoho.refresh_token')
        );
        $responseRefresh = Http::asForm()->post(config('zoho.token_url'), $paramsRefresh)->json();
        config([
            'zoho.access_token' => $responseRefresh['access_token'],
        ]);
    }

    public function checkToken()
    {
        $responseDeals = Http::withHeaders(['Authorization' => 'Zoho-oauthtoken ' . config('zoho.access_token')])
            ->get('https://www.zohoapis.com/crm/v2/Deals')
            ->json();

        if (isset($responseDeals['code']) && $responseDeals['code'] === config('zoho.invalid_token')) {
            $this->refreshToken();
        }
    }

}
